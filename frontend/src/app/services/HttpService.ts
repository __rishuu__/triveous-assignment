import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Bookmarks, Tags} from '../components/model';
import {Observable} from 'rxjs';
import {UIupdaterService} from './UIupdaterService';
import {conditionallyCreateMapObjectLiteral} from '@angular/compiler/src/render3/view/util';

@Injectable({providedIn: 'root'})
export class HttpService {
  private domainName = 'http://localhost:8000/' + 'api/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient, private uiUpdaterService: UIupdaterService) {}

  getBookmarks(): Observable<Bookmarks[]> {
    return this.http.get<Bookmarks[]>(this.domainName + 'bookmarks');
  }

  getTags(): Observable<Tags[]> {
    return this.http.get<Tags[]>(this.domainName + 'tags');
  }

  addBookmarks(entry: any): Observable<any> {
    return this.http.post(this.domainName + 'bookmarks/', JSON.stringify(entry), this.httpOptions);
  }

  addTags(entry: any): Observable<any> {
    return this.http.post(this.domainName + 'tags/', JSON.stringify(entry), this.httpOptions);
  }

  deleteTag(idx: number): Observable<any> {
    return this.http.delete(this.domainName + 'tags/' + String(idx));
  }

  deleteBookmark(idx: number): Observable<any> {
    return this.http.delete(this.domainName + 'bookmarks/' + String(idx));
  }

  updateBookmarks(idx: number, entry: any) : Observable<any> {
    return this.http.put(this.domainName + 'bookmarks/' + String(idx) + '/', JSON.stringify(entry), this.httpOptions);
  }
}
