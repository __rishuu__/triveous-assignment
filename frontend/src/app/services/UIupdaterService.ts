import {Injectable} from '@angular/core';
import {Bookmarks, Tags} from '../components/model';

@Injectable({
  providedIn: 'root'
})
export class UIupdaterService {
  public bookmarks: Bookmarks[];
  public tags: Tags[];

  setBookmarks(bookmarks: Bookmarks[]): void{
    this.bookmarks = bookmarks;
  }

  setTags(tags: Tags[]): void {
    this.tags = tags;
  }

  addBookmarks(entry: Bookmarks): void{
    this.bookmarks.push(entry);
  }

  addTags(entry: Tags): void{
    this.tags.push(entry);
  }

  deleteBookmark(idx: number): void{
    this.bookmarks.splice(idx, 1);
  }

  deleteTag(idx: number): void{
    this.tags.splice(idx, 1);
  }
}
