export class Tags {
  public id:number;
  public uuid: string;
  public title: string;
  public timeCreated: string;
  public timeUpdated: string;

  constructor(jsonResp) {
    this.id = jsonResp['id'];
    this.uuid = jsonResp['uuid'];
    this.title = jsonResp['title'];
    this.timeCreated = jsonResp['time_created'];
    this.timeUpdated = jsonResp['time_updated'];
  }
}

export class Bookmarks {
  public id: number;
  public uuid: string;
  public link: string;
  public title: string;
  public timeCreated: string;
  public timeUpdated: string;
  public publisher: string;
  public tags: Tags[] = [];

  constructor(jsonResp) {
    this.id = jsonResp['id'];
    this.uuid = jsonResp['uuid'];
    this.link = jsonResp['link'];
    this.title = jsonResp['title'];
    this.timeCreated = jsonResp['time_created'];
    this.timeUpdated = jsonResp['time_updated'];
    this.publisher = jsonResp['publisher'];

    for (let tag of jsonResp['tags']){
      let obj = new Tags(tag);
      this.tags.push(obj);
    }

  }
}
