import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/HttpService';
import {Tags} from '../model';
import {FormControl, FormGroup} from '@angular/forms';
import {UIupdaterService} from '../../services/UIupdaterService';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.css']
})
export class TagsListComponent implements OnInit {
  tags: Tags[];
  tagsForm: FormGroup;
  constructor(private http: HttpService, private uiService: UIupdaterService) { }

  ngOnInit(): void {
    this.http.getTags().subscribe(
      resp => {
        this.tags = resp;
        this.uiService.setTags(this.tags);
      }
    );

    this.tagsForm = new FormGroup({
      title: new FormControl()
    });

  }

  onSubmit() {
    let title = this.tagsForm.getRawValue().title;
    let tag = {
      "title": title,
      "time_updated": String(new Date().valueOf()),
      "time_created": String(new Date().valueOf()),
      "uuid": Math.random().toString(36).substring(7),
      // "id": this.tags[this.tags.length - 1].id + 1
    };

    this.http.addTags(tag).subscribe(
      resp => {
        tag["id"] = resp.id;
        this.uiService.addTags(new Tags(tag));
        this.tags = this.uiService.tags;
        window.location.reload();
      }, err => {console.log(err)}
    );

  }

  onDelete(actual: number, idx: number){
    this.uiService.deleteTag(idx);
    this.tags = this.uiService.tags;
    this.http.deleteTag(actual).subscribe(resp => {console.log(resp);    window.location.reload();}, err => {console.log(err)});
  }

}
