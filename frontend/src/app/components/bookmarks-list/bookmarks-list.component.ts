import { Component, OnInit } from '@angular/core';
import {Bookmarks, Tags} from '../model';
import {HttpService} from '../../services/HttpService';
import {FormControl, FormGroup} from '@angular/forms';
import {UIupdaterService} from '../../services/UIupdaterService';

@Component({
  selector: 'app-bookmarks-list',
  templateUrl: './bookmarks-list.component.html',
  styleUrls: ['./bookmarks-list.component.css']
})
export class BookmarksListComponent implements OnInit {

  bookmarksList: Bookmarks[];
  bookmarksForm: FormGroup;
  tags: Tags[];
  constructor(private httpService: HttpService, private uiService: UIupdaterService) { }

  ngOnInit(): void {
    this.httpService.getTags().subscribe(resp => {this.tags = resp; this.uiService.tags = this.tags;});
    this.httpService.getBookmarks().subscribe(
      response => {
        this.bookmarksList = response;
        this.uiService.bookmarks = this.bookmarksList;
      }
    );

    this.bookmarksForm = new FormGroup({
      title: new FormControl(),
      publisher: new FormControl(),
      link: new FormControl(),
      tags: new FormControl()
    });
  }

  get tagsName() {
    return this.bookmarksForm.get('tags');
  }

  onSubmit(){
    // console.log(this.bookmarksForm.getRawValue());
    let val = this.bookmarksForm.getRawValue();
    let tags = [];
    for (let i of this.bookmarksForm.getRawValue().tags){
      let t = this.tags[i];
      tags.push({
        "title": t.title,
        "time_updated": t['time_created'],
        "time_created": t['time_updated'],
        "uuid": t.uuid,
        "id" : t.id
      });
    }

    let bookmark = {
      "title" : val.title,
      "publisher" : val.publisher,
      "time_updated": String(new Date().valueOf()),
      "time_created": String(new Date().valueOf()),
      "uuid": Math.random().toString(36).substring(10),
      "link": val.link,
      "tags" : tags
    };

    this.uiService.addBookmarks(new Bookmarks(bookmark));
    this.bookmarksList = this.uiService.bookmarks;

    this.httpService.addBookmarks(bookmark).subscribe(
      resp => {
        bookmark["id"] = resp.id;
        window.location.reload();
         },
      error => {console.log(error); }
    );
  }

  onDelete(actual: number, idx: number){
    this.uiService.deleteBookmark(idx);
    this.bookmarksList = this.uiService.bookmarks;
    this.httpService.deleteBookmark(actual).subscribe(resp => {console.log(resp);         window.location.reload();
    }, err => {console.log(err);});
  }

  onDeleteTag(idx2: number, idx: number){
    let bk;
    let bookmarks_index: number = 0;
    for (let kk of this.bookmarksList){
      if (kk.id === idx2){
        bk = kk;
        break;
      }
      bookmarks_index++;
    }
    let val = bk;
    let index = bk.id;
    let tags = [];
    let looper = 0;
    for (let i of bk.tags){
      if (looper === idx){
        looper++;
        continue;
      }
      let t = i;
      tags.push({
        "title": t.title,
        "time_updated": t['time_created'],
        "time_created": t['time_updated'],
        "uuid": t.uuid
      });
      looper++;
    }

    let bookmark = {
      "id" : val.id,
      "title" : val.title,
      "publisher" : val.publisher,
      "time_updated": val['time_created'],
      "time_created": val['time_updated'],
      "uuid": val.uuid,
      "link": val.link,
      "tags" : tags
    };

    console.log(bookmark)
    this.httpService.updateBookmarks(index, bookmark).subscribe(
      resp => {

        this.bookmarksList[bookmarks_index].tags.splice(idx, 1);
        this.uiService.bookmarks = this.bookmarksList;
        window.location.reload();
      },
      err => {
        console.log(err);
      }
    );

  }

}
