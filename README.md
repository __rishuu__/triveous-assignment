# README #

1. This repo contains two folders
	a. backend: backend API in django + mongoDB
	b. frontend: frontend in angular.

2. Instructions to run.
	1. install mongodb
	2. pip install -r requirements.txt
	3. in the backend/Bookmarks folder run $ ./manage,py makemigrations && ./manage.py migrate
	4. to prefill the mongdb use $ ./manage.py loaddata *.json
	5. to run the backend api $ ./manage,py runserver [make sure it runs on port 8000]
	6. to run the frontend app go in the frontend folder and run $ npm install (after instaling node.js)
	7. then run ng serve to run the angular server [default port : 4200]
	8. Once both mongodb and django are running angular can interact with them easily.
	9. if Only API is to be tested then frontend folder in the repo can be ignored and only django and mongodb is required.

3. I'm trying to set up hosting for the whole thing (this is my first time trying to deploy something)
	1. If I'm able to do it then, I will surely provide the link.
	2. But, if I am not able to do it in the time-limit then, we can always use a tunneling service to generate the link (and of course the time at which I must run the code on my laptop) to access the whole-setup as a workaround.