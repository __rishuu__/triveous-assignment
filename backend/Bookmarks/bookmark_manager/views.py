from rest_framework import viewsets, status, filters
from .serializers import BookmarksSerializer, TagsSerializer
from .models import Bookmarks, Tags

# Create your views here.
class BookmarksView(viewsets.ModelViewSet):
    serializer_class = BookmarksSerializer
    queryset = Bookmarks.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']

class TagsView(viewsets.ModelViewSet):
    serializer_class = TagsSerializer
    queryset = Tags.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']