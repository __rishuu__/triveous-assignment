from rest_framework import serializers
from .models import Bookmarks, Tags

class TagsSerializer(serializers.ModelSerializer):
    """
        Tags serializer
    """
    uuid = serializers.CharField()
    title = serializers.CharField(max_length=20)
    time_created = serializers.CharField()
    time_updated = serializers.CharField()

    class Meta:
        model = Tags
        fields = ('id', 'uuid', 'title', 'time_created', 'time_updated')

class BookmarksSerializer(serializers.ModelSerializer):
    """
        Bookmarks serializer
    """
    uuid = serializers.CharField()
    link = serializers.CharField()
    title = serializers.CharField(max_length=50)
    time_created = serializers.CharField()
    time_updated = serializers.CharField()
    publisher = serializers.CharField(max_length=30)
    tags = serializers.ListField(child=TagsSerializer())

    class Meta:
        model = Bookmarks
        fields = ('id', 'uuid', 'link', 'title', 'time_created', 'time_updated', 'publisher', 'tags')