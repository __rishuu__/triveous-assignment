from django.apps import AppConfig


class BookmarkManagerConfig(AppConfig):
    name = 'bookmark_manager'
