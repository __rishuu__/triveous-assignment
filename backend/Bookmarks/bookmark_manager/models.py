from djongo import models

# Create your models here.
class Tags(models.Model):
    """
        Tags model
    """
    uuid = models.TextField(unique=True)
    # tags are usually of short length
    title = models.TextField(max_length=20, unique=True)
    time_created = models.TextField()
    # confusion ? if no updation api is to be provided then creation is equal to updation
    time_updated = models.TextField()

class Bookmarks(models.Model):
    """
        Base model class
    """
    uuid = models.TextField(unique=True)
    link = models.TextField(unique=True)
    title = models.TextField(max_length=50)
    time_created = models.TextField()
    time_updated = models.TextField()
    publisher = models.TextField(max_length=30)
    tags = models.ListField(Tags)